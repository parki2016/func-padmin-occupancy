package au.com.parki.admin.occupancy.funqy;

import au.com.parki.ParkiTokenUtil;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

@Dependent
public class ParkingOccupancyConfig {

    @Produces
    public ParkiTokenUtil parkiTokenUtil() {
        return new ParkiTokenUtil();
    }
}
