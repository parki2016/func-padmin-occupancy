package au.com.parki.admin.occupancy.funqy.dto;

import au.com.parki.admin.occupancy.funqy.model.ParkingOccupancyDetail;

import java.util.List;

public class ParkingOccupancyResponse {

    private Long currentOccupancy;
    private Long averageOccupancy;
    private Double maxOccupancy;
    private List<ParkingOccupancyDetail> occupancyDetailsPerHour;
    private List<ParkingOccupancyDetail> occupancyDetailsPer15Minutes;

    public Long getCurrentOccupancy() { return currentOccupancy; }
    public void setCurrentOccupancy(Long currentOccupancy) { this.currentOccupancy = currentOccupancy; }

    public Long getAverageOccupancy() { return averageOccupancy; }
    public void setAverageOccupancy(Long averageOccupancy) { this.averageOccupancy = averageOccupancy; }

    public Double getMaxOccupancy() { return maxOccupancy; }
    public void setMaxOccupancy(Double maxOccupancy) { this.maxOccupancy = maxOccupancy; }

    public List<ParkingOccupancyDetail> getOccupancyDetailsPerHour() { return occupancyDetailsPerHour; }
    public void setOccupancyDetailsPerHour(List<ParkingOccupancyDetail> occupancyDetailsPerHour) {
        this.occupancyDetailsPerHour = occupancyDetailsPerHour;
    }

    public List<ParkingOccupancyDetail> getOccupancyDetailsPer15Minutes() { return occupancyDetailsPer15Minutes; }
    public void setOccupancyDetailsPer15Minutes(List<ParkingOccupancyDetail> occupancyDetailsPer15Minutes) {
        this.occupancyDetailsPer15Minutes = occupancyDetailsPer15Minutes;
    }
}
