package au.com.parki.admin.occupancy.funqy.model;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.NumberFormat;

public class ParkingOccupancyDetail {

    private String timeOfDay;
    private Integer occupiedParking;
    private Double percentageOccupied;

    private static final DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("HH:mm");


    public static ParkingOccupancyDetail getData(ParkingOccupancy parkingOccupancy) {
        ParkingOccupancyDetail detail = new ParkingOccupancyDetail();
        detail.setTimeOfDay(timeFormatter.print(parkingOccupancy.getDateTime()));
        Integer occupied = parkingOccupancy.getCapacity() - parkingOccupancy.getAvailableSpaces();
        detail.setOccupiedParking(occupied>=0?occupied:0);
        Double percentage = Double.valueOf(detail.getOccupiedParking())/Double.valueOf(parkingOccupancy.getCapacity());
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        detail.setPercentageOccupied(Double.valueOf(nf.format(percentage*100)));
        return detail;
    }

    public String getTimeOfDay() { return timeOfDay; }
    public void setTimeOfDay(String timeOfDay) { this.timeOfDay = timeOfDay; }

    public Integer getOccupiedParking() { return occupiedParking;  }
    public void setOccupiedParking(Integer occupiedParking) { this.occupiedParking = occupiedParking; }

    public Double getPercentageOccupied() { return percentageOccupied; }
    public void setPercentageOccupied(Double percentageOccupied) { this.percentageOccupied = percentageOccupied; }
}
