package au.com.parki.admin.occupancy.funqy.dto;

import java.sql.Date;

public class ParkingOccupancyRequest {

    private String token;
    private String date;
    private Long zone;

    public String getToken() { return token; }
    public void setToken(String token) { this.token = token; }

    public Date getDate() { return Date.valueOf(date); }
    public void setDate(String date) { this.date = date; }

    public Long getZone() { return zone; }
    public void setZone(Long zone) { this.zone = zone; }
}
