package au.com.parki.admin.occupancy.funqy;

import au.com.parki.ParkiTokenUtil;
import au.com.parki.admin.occupancy.funqy.dto.ParkingOccupancyRequest;
import au.com.parki.admin.occupancy.funqy.dto.ParkingOccupancyResponse;
import au.com.parki.admin.occupancy.funqy.service.ParkingOccupancyService;
import io.quarkus.funqy.Funq;

import javax.inject.Inject;
import javax.transaction.Transactional;

public class ParkingOccupancyFunction {

    @Inject
    ParkiTokenUtil tokenUtil;

    @Inject
    ParkingOccupancyService parkingOccupancyService;

    @Transactional
    @Funq
    public ParkingOccupancyResponse getParkingOccupancyDetails(ParkingOccupancyRequest request) {

        String tokenStr = request.getToken();
        System.out.println("GOT Token : " + tokenStr);
        Long org = tokenUtil.getParkiUserToken(tokenStr).getOrg();

        return parkingOccupancyService.getParkingOccupancyDetails(org, request.getZone(), request.getDate());
    }
}
