package au.com.parki.admin.occupancy.funqy.model;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "ipark_occupancy")
public class ParkingOccupancy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "available_spaces")
    private Integer availableSpaces;

    @Column
    private Integer capacity;

    @Column(name = "date_time")
    private Timestamp dateTime;

    @Column(name = "site_id")
    private Integer siteId;

    @Column
    private Long version;

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Integer getAvailableSpaces() { return availableSpaces; }
    public void setAvailableSpaces(Integer availableSpaces) { this.availableSpaces = availableSpaces; }

    public Integer getCapacity() { return capacity; }
    public void setCapacity(Integer capacity) { this.capacity = capacity; }


    public DateTime getDateTime() {
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        return DateTime.parse(dateFormatter.print(dateTime.getTime()));
    }
    public void setDateTime(Timestamp dateTime) { this.dateTime = dateTime; }

    public Integer getSiteId() { return siteId; }
    public void setSiteId(Integer siteId) { this.siteId = siteId; }

    public Long getVersion() { return version; }
    public void setVersion(Long version) { this.version = version; }
}
