package au.com.parki.admin.occupancy.funqy.service;

import au.com.parki.admin.occupancy.funqy.dto.ParkingOccupancyResponse;
import au.com.parki.admin.occupancy.funqy.model.ParkingOccupancy;
import au.com.parki.admin.occupancy.funqy.model.ParkingOccupancyDetail;
import io.quarkus.runtime.Startup;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Startup
@Singleton
public class ParkingOccupancyService {

    @Inject
    EntityManager em;


    public ParkingOccupancyResponse getParkingOccupancyDetails(Long organisation, Long zone, Date dateInput) {

        ParkingOccupancyResponse response = new ParkingOccupancyResponse();
        List<ParkingOccupancy> fullList = getParkingOccupancyList(organisation, zone, dateInput);
        response.setOccupancyDetailsPer15Minutes(fullList.stream().map(ParkingOccupancyDetail::getData).collect(Collectors.toList()));
        response.setCurrentOccupancy(getCurrentOccupancy(zone));

        List<ParkingOccupancyDetail> perHour = new ArrayList<>();
        for (ParkingOccupancy occupancy : fullList) {
            if (occupancy.getDateTime().getMinuteOfHour()==0) perHour.add(ParkingOccupancyDetail.getData(occupancy));
        }
        response.setOccupancyDetailsPerHour(perHour);

        response.setAverageOccupancy((long) Math.ceil(perHour.stream().mapToDouble(ParkingOccupancyDetail::getOccupiedParking).average().orElse(0.0)));
        response.setMaxOccupancy(perHour.stream().mapToDouble(ParkingOccupancyDetail::getPercentageOccupied).max().orElse(0));

        return response;
    }

    private static final String PARKING_OCCUPANCY = "SELECT o.id, o.available_spaces, o.capacity, " +
            "CONVERT_TZ(o.date_time, 'UTC', org.timezone) AS date_time, o.site_id, o.version " +
            "FROM ipark_occupancy o " +
            "INNER JOIN ipark_config c ON o.site_id = c.site_id " +
            "INNER JOIN zone z ON c.zone_fk = z.id " +
            "INNER JOIN organisation org ON org.id = z.organisation_fk " +
            "WHERE DATE_FORMAT(CONVERT_TZ(o.date_time, 'UTC', org.timezone),'%Y-%m-%d') = DATE_FORMAT(:dateInput,'%Y-%m-%d') " +
            "AND org.id = :organisationId  " +
            "AND z.id = :zoneId " +
            "AND z.car_park_type_fk = 2 " +
            "AND c.active = 1 " +
            "ORDER BY CONVERT_TZ(o.date_time, 'UTC', org.timezone) ";
    private static final String DATE = "dateInput";
    private static final String ORGANISATION = "organisationId";
    private static final String ZONE = "zoneId";
    private List<ParkingOccupancy> getParkingOccupancyList(Long organisation, Long zone, Date dateInput) {
        return em.createNativeQuery(PARKING_OCCUPANCY, ParkingOccupancy.class)
                .setParameter(DATE, dateInput)
                .setParameter(ORGANISATION, organisation)
                .setParameter(ZONE, zone)
                .getResultList();
    }

    private static final String TIME_ZONE = "SELECT " +
            "(COALESCE(z.numberOfBays, 0) - COALESCE(z.available_spaces, 0)) AS occupied " +
            "FROM zone z " +
            "WHERE z.id = :zoneId ";
    public Long getCurrentOccupancy(Long zone) {
        Object currentOccupancy = em.createNativeQuery(TIME_ZONE)
                .setParameter(ZONE, zone)
                .getSingleResult();
        if(currentOccupancy != null) return Long.valueOf(currentOccupancy.toString());
        return null;
    }

}
