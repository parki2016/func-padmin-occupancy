package au.com.parki.admin.occupancy.funqy;

import au.com.parki.admin.occupancy.funqy.dto.ParkingOccupancyRequest;
import au.com.parki.admin.occupancy.funqy.dto.ParkingOccupancyResponse;
import io.quarkus.amazon.lambda.test.LambdaClient;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class FunqyTest {
    @Test
    public void testSimpleLambdaSuccess() throws Exception {
        ParkingOccupancyRequest in = new ParkingOccupancyRequest();
        in.setToken("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjZHN3QGNhcmVwYXJrLmNvbS5hdSIsImlzcyI6IlBhcmtpQWRtaW4iLCJleHAiOjE2MzM2NTQwNTEsIm9yZyI6Mn0.q58HuYM9qk4wKyG-_ELT_ThubKJFCTkb_lihIM-1-o4");
        //orgId = 100004, 2021-09-20, 2040
        //orgId = 2, any, 484/2220
        in.setDate("2021-09-20");
        in.setZone(Long.valueOf(2040));
        ParkingOccupancyResponse out = LambdaClient.invoke(ParkingOccupancyResponse.class, in);
        System.out.println("max Occupancy: " + out.getMaxOccupancy());
        System.out.println("current Occupancy: " + out.getCurrentOccupancy());
        System.out.println("average Occupancy: " + out.getAverageOccupancy());

        for (int i = 0; i < out.getOccupancyDetailsPerHour().size() ; i++) {
            System.out.print("Data: " + i);
            System.out.print(" Time: " + out.getOccupancyDetailsPerHour().get(i).getTimeOfDay());
            System.out.print(" Occupied: " + out.getOccupancyDetailsPerHour().get(i).getOccupiedParking());
            System.out.println(" Percentage: " + out.getOccupancyDetailsPerHour().get(i).getPercentageOccupied());
        }
        Assertions.assertTrue(out!=null);
    }
}
